import React, { Suspense } from "react";

import "./Counter.scss";
const CounterMain = React.lazy(() =>
  import("../../components/CounterMain/CounterMain")
);

function Counter() {
  // throw Error("Something went wrong in Counter");

  return (
    <div>
      <Suspense fallback={<h1>Loading...</h1>}>
        <CounterMain />
      </Suspense>
    </div>
  );
}

export default Counter;
