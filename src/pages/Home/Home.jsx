import React, { Suspense } from "react";

import { HomeText, WithStyling } from "../../components";
import "./Home.scss";

const HomeTextt = WithStyling(HomeText);

const Home = () => {
  return (
    <div className="home">
      <div className="container">
        <Suspense fallback={<h1>Loading...</h1>}>
          <HomeTextt />
        </Suspense>
      </div>
    </div>
  );
};

export default Home;
