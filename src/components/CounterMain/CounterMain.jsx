import React, { useState } from "react";

import "./CounterMain.scss";

function CounterMain() {
  const [count, setCount] = useState(1);

  if (count == 0) {
    throw new Error("Something went wrong in Counter");
  }

  function handleIncrement() {
    setCount((prvCount) => prvCount + 1);
  }

  function handleDecrement() {
    setCount((prvCount) => prvCount - 1);
  }

  return (
    <div className="counter">
      <div className="container">
        <div className="counter-content">
          <button onClick={handleDecrement}>-</button>
          <h1>{count}</h1>
          <button onClick={handleIncrement}>+</button>
        </div>
      </div>
    </div>
  );
}

export default CounterMain;
