import Nav from "./Nav/Nav";
import ErrorBoundary from "./ErrorBoundary/ErrorBoundary";
import WithStyling from "./WithStyling/WithStyling";
import HomeText from "./HomeText/HomeText";
import Modal from "./Modal/Modal";

export { Nav, ErrorBoundary, WithStyling, HomeText, Modal };
