import React, { forwardRef, useImperativeHandle, useState } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import "./Modal.scss";

function Modal(props, ref) {
  const [display, setDisplay] = useState(false);

  useImperativeHandle(ref, () => {
    return {
      openModal: () => open(),
      closeModal: () => close(),
      toggleModal: () => toggle(),
    };
  });

  function open() {
    setDisplay(true);
  }

  function close() {
    setDisplay(false);
  }

  function toggle() {
    setDisplay(!display);
  }

  if (display) {
    return ReactDOM.createPortal(
      <div className="modal-wrapper">
        <div className="modal-backdrop" onClick={close} />
        <div className="modal-box">{props.children}</div>
      </div>,
      document.getElementById("modal-root")
    );
  }

  return null;
}

Modal.propTypes = {
  children: PropTypes.node,
};

export default forwardRef(Modal);
