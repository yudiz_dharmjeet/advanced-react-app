import React from "react";

import "./WithStyling.scss";

function WithStyling(WrappedComponent) {
  return class Wrapped extends React.Component {
    render() {
      return (
        <WrappedComponent
          {...this.props}
          style={{
            fontWeight: 600,
            color: "black",
            background: "#51e1ed",
            display: "inline-block",
            padding: "8px 16px",
            borderRadius: "8px",
            boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
          }}
        />
      );
    }
  };
}

export default WithStyling;
