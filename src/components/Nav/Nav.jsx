import React, { useRef } from "react";
import { NavLink } from "react-router-dom";
import Modal from "../Modal/Modal";

import "./Nav.scss";

function Nav() {
  const modalRef = useRef();

  function openModal() {
    modalRef.current.openModal();
  }

  return (
    <>
      <div className="nav">
        <div className="container">
          <div className="nav__logo">Hello</div>
          <ul className="nav__list">
            <li className="nav__item">
              <NavLink
                className={(navData) => (navData.isActive ? "active-link" : "")}
                to="/"
              >
                Home
              </NavLink>
            </li>
            <li className="nav__item">
              <NavLink
                className={(navData) => (navData.isActive ? "active-link" : "")}
                to="/counter"
              >
                Counter
              </NavLink>
            </li>
            <li className="nav__item" onClick={openModal}>
              Modal
            </li>
          </ul>
        </div>
      </div>

      <Modal ref={modalRef}>
        <h1 className="modal-h1">Welcome Everyone....</h1>
        <p className="modal-p">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis nobis
          quae ab nulla? Quas totam fugit consectetur necessitatibus sunt iusto
          magni vitae perspiciatis. Iste corrupti saepe rem officiis ipsum! A!
        </p>
        <button
          className="modal-btn"
          onClick={() => modalRef.current.closeModal()}
        >
          Close Modal
        </button>
      </Modal>
    </>
  );
}

export default Nav;
