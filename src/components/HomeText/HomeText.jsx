import React from "react";

import "./HomeText.scss";

function HomeText({ ...props }) {
  return <h1 {...props}>HomeText</h1>;
}

export default HomeText;
