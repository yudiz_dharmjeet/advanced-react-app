import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { ErrorBoundary, Nav } from "./components";
import { Counter, Home } from "./pages";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route
          path="/counter"
          element={
            <ErrorBoundary>
              <Counter />
            </ErrorBoundary>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
